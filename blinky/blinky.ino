#include "LPD8806.h"  // Arduino users: install this library first
#include "SPI.h"

int dataPin = 5;
int clockPin = 6;
int nLEDs = 16;
int chase_delay = 100; // milliseconds

// LPD8806 class
// First parameter is the number of LEDs in the strand.
// Our strips have 48 LEDs per meter but you can extend or cut the strip.
// Next two parameters are SPI data and clock pins:
LPD8806 strip = LPD8806(nLEDs, dataPin, clockPin);

// ANIMATIONS

// Takes a color (uint32_t c)which can be constructed using strip.Color(r,g,b),
// and a "between blinks" delay given as milliseconds (uint8_t wait)
// Produces an animation which blinks each LED in order using the given color.
void colorChase(uint32_t c, uint8_t wait){
  for(int i=0; i<strip.numPixels(); i++){strip.setPixelColor(i, 0);} // all black pixels

  // Then display one pixel at a time:
  for(int i=0; i<strip.numPixels(); ++i){
    strip.setPixelColor(i, c); // Set new pixel 'on'
    strip.show();              // Write to the LED strip
    strip.setPixelColor(i, 0); // Erase old pixel (set its color to 0), but don't refresh!
    delay(wait);  // keep the old pixel on for (wait) milliseconds
  }
  strip.show(); // Refresh to turn off last pixel
}


// Like colorChase, but without erasing the old pixel
void colorWipe(uint32_t c, uint8_t wait){
  for(int i=0; i<strip.numPixels(); ++i){
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

// write a 16-bit value to the LED strip in binary format
void write16(uin32_t c, uint16_t val){
  for(int i=0; i<strip.numPixesl(); ++i){strip.setPixelColor(i, 0);} // all black pixels
  // display the 16-bit value as binary (1==on, 0==off)
  for(int i=0; i<16; ++i){
    strip.setPixelColor(val & (1<<i), c); // set the ith pixel to the value (1 or 0) of the ith bit of val
  }
  strip.show(); // update whole strip at once
}

// same as write16, but keep the previous color
void overwrite16(uint32_t c, uint16_t val){
  // display the 16-bit value as binary (1==on, 0==off)
  for(int i=0; i<16; ++i){
    strip.setPixelColor(val & (1<<i), c); // set the ith pixel to the value (1 or 0) of the ith bit of val
  }
  strip.show(); // update whole strip at once
}

void setup(){
  strip.begin(); // Start up the LED strip
  strip.show(); // Update the strip, to start they are all 'off'
}

void loop(){
  // animations
  colorChase(strip.Color(127,   0,   0), chase_delay); // Red
  colorWipe(strip.Color(  0, 127,   0), chase_delay); // Green
  colorChase(strip.Color(  0,   0, 127), chase_delay); // Blue
  delay(500);
  colorWipe(strip.Color(127, 127,   0), chase_delay); // Yellow
  colorWipe(strip.Color(127,   0, 127), chase_delay); // Violet
  colorChase(strip.Color(127, 127, 127), chase_delay); // White
  delay(500);

  // increment a 16-bit counter by 1
  for(int i=0; i<50; ++i){
    write16(strip.Color(127,80,80), i);
    delay(chase_delay);
  }

  // increment a 16-bit counter by 10
  for(int i=0; i<500; i+=10){
    write16(strip.Color(127,80,80), i);
    delay(chase_delay);
  }

  // test overwrite capability
  write16(strip.color(40,40,40), 0xffff); // gray
  delay(chase_delay);
  overwrite16(strip.color(0,0,127), 0xf0f0); // first 4 bits of each byte should now be blue
  delay(chase_delay);
  overwrite16(strip.color(0,127,0), 0xd0d0); // first 2 bits of each byte should now be green
  delay(chase_delay);
  overwrite16(strip.color(127,0,0), 0x8080); // first bit of each byte should now be red
}
