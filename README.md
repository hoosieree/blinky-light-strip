# Blinky Light Strip
![](demo.png)

# C++ bitwise utilities
Some of these may be useful when writing Arduino code that manipulates the individual bits of numbers.

## Get, Set, or Clear a particular bit

```c++
uint16_t y = 14327; // in binary: (0 0 1 1 0 1 1 1 1 1 1 1 0 1 1 1)
uint16_t x0  = 0;   // location of bit 0:  (_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ x)
uint16_t x3  = 3;   // location of bit 3:  (_ _ _ _ _ _ _ _ _ _ _ _ x _ _ _)
uint16_t x15 = 15;  // location of bit 15: (x _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)

// return 1 if the xth bit of y is 1, else 0
// examples
// get_nth_bit(x0, y); // returns 1
// get_nth_bit(x3, y); // returns 0
// get_nth_bit(x15, y); // returns 0
uint16_t get_nth_bit(uint16_t x, uint16_t y){
  return (y & (1 << x)) >> x;
}

// return y with xth bit set to 1 (does not modify original y)
// examples
// set_nth_bit(x0, y);  // returns 14327 (0 0 1 1 0 1 1 1 1 1 1 1 0 1 1 1)
// set_nth_bit(x3, y);  // returns 14335 (0 0 1 1 0 1 1 1 1 1 1 1 1 1 1 1)
// set_nth_bit(x15, y); // returns 47095 (1 0 1 1 0 1 1 1 1 1 1 1 0 1 1 1)
uint16_t set_nth_bit(uint16_t x, uint16_t y){
  return y | (1 << x);
}

// return y with xth bit set to 0 (does not modify original y)
// examples
// clear_nth_bit(x0, y);  // returns 14326 (0 0 1 1 0 1 1 1 1 1 1 1 0 1 1 0)
// clear_nth_bit(x3, y);  // returns 14327 (0 0 1 1 0 1 1 1 1 1 1 1 0 1 1 1)
// clear_nth_bit(x15, y); // returns 14327 (0 0 1 1 0 1 1 1 1 1 1 1 0 1 1 1)
uint16_t clear_nth_bit(uint16_t x, uint16_t y){
  return y & ~(1 << x);
}
```

## Get, Set, or Clear several bits at once
Less explanation here, read up on C++ bitwise operations and "numeric literal" value syntax, or ask questions in class.

```c++
// get
uint16_t get_dest_bits(uint16_t y){
  return y & 0x38; // alternatively: return y & 56;
}

// set lower 3 bits of y to entire value of x (up to 1 1 1)
uint16_t set_lower_three_bits(uint16 x, uint16_y){
  return y & (x & 7);  // only uses lowest 3 bits of x
}

// set "dest" bits of y to entire value of x
uint16_t set_dest_bits(uint16_t x, uint16_t y){
  return y | get_dest_bits(x);
}

// clear bits of y which are already cleared in x
uint16_t clear_dest_bits(uint16_t x, uint16_t y){
  return y & (~56 | x);
}
```

You may wonder what this `uint16_t` means.
It is shorthand for "unsigned integer type taking up 16 bits".
The type is defined in the standard library file `stdint.h`.  The Arduino IDE includes it automatically in the main `sketchname.ino` file, but external libraries need to include it themselves:

`#include <stdint.>`

There are other ways of writing this type:

unsigned short
unsigned int

But beware that `unsigned int` is only 16 bits if your CPU is also 16 bits (which is true for some but not all Arduinos).  To avoid confusion, I prefer `uint16_t`.

`stdint.h` also includes these variants which you may find useful:

* `uint8_t` unsigned 8-bit value (0 to 255)
* `int8_t` signed 8-bit value (-128 to 127)
* `uint16_t` unsigned 16-bit value (0 to 65535)
* `int16_t` signed 16-bit value (-32768 to 32767)
* `uint32_t` unsigned 32-bit value (0 to 2^32 - 1)
* `int32_t` signed 32-bit value (-2^31 to 2^31 - 1)

